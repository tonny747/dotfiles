#!/bin/bash
# Author: nnoell <nnoell3@gmail.com>
# Depends: dzen2-xft-xpm-xinerama-svn
# Desc: dzen2 bar for XMonad, ran within xmonad.hs via spawnPipe

#Layout
BAR_H=9
BIGBAR_W=65
WIDTH=600
HEIGHT=16
X_POS=700
Y_POS=800

#Colors and font
FONT="-*-montecarlo-medium-r-normal-*-11-*-*-*-*-*-*-*"
DZEN_BG="#020202"
DZEN_FG="#9d9d9d"
DZEN_FG2="#444444"
CRIT="#99cc66"
BAR_FG="#3955c4"
BAR_BG="#363636"
COLOR_SEP=$DZEN_FG2

#Options
#Conky
CONKYFILE="${HOME}/.conkyrc"
IFS='|'
CPUTemp=0
GPUTemp=0
CPULoad0=0
CPULoad1=0
CPULoad2=0
CPULoad3=0
MpdInfo=0
MpdRandom="Off"
MpdRepeat="Off"
CONKYFILE="${HOME}/.conkyrc"
CONKYINTERVAL=20
INTERVAL=20
WIFISIGNAL=0


printBattery() {
	BatPresent=$(acpi -b | wc -l)
	ACPresent=$(acpi -a | grep -c on-line)
	if [[ $BatPresent == "0" ]]; then
		echo -n "^fg($DZEN_FG2)AC ^fg($BAR_FG)on ^fg($DZEN_FG2)BAT ^fg($BAR_FG)off"
		return
	else
		RPERC=$(acpi -b | awk '{print $4}' | tr -d "%,")
		REM=$(acpi -b | awk '{print $5}')
		echo -n "^fg($DZEN_FG2)BAT "
		if [[ $ACPresent == "1" ]]; then
			echo -n "$(echo $RPERC | dzen2-gdbar -fg $BAR_FG -bg $BAR_BG -h $BAR_H -w $BIGBAR_W -s o -ss 1 -sw 2 -nonl)"
		else
			echo -n "$(echo $RPERC | dzen2-gdbar -fg $CRIT -bg $BAR_BG -h $BAR_H -w $BIGBAR_W -s o -ss 1 -sw 2 -nonl)"
		fi
		echo -n " ^fg($DZEN_FG)$RPERC%"
                echo -n " $REM"
	fi
	return
}



printWifiInfo() {
	[[ $WIFIDOWN -ne "1" ]] && WIFISIGNAL=$(wicd-cli --wireless -d | grep Quality | awk '{print $2}')
	echo -n "^fg($DZEN_FG1)"
	echo -n "$Essid "
	echo -n "$(echo $WIFISIGNAL | dzen2-gdbar -fg $BAR_FG -bg $BAR_BG -h $BAR_H -w $BIGBAR_W -s o -ss 1 -sw 2 -nonl) "
	echo -n "^fg()$WIFISIGNAL% "
	return
}

printSpeeds() {
    echo -n "^fg($DZEN_FG) $Down"
    echo -n " ^fg($COLOR_SEP)| ^fg($DZEN_FG) $Up ^fg() "
}
printSpace() {
	echo -n " ^fg($COLOR_SEP)|^fg() "
	return
}

printBottomBar() {
	while true; do
            read Down Up Essid Bar
	    printBattery
	    printSpace
            printSpeeds
#	    printSpace
#	    printWifiInfo
            echo
	    sleep 2
	done
	return
}

#Print all and pipe into dzen2
conky -c $CONKYFILE | printBottomBar | dzen2 -x $X_POS -y $Y_POS -w $WIDTH -h $HEIGHT -fn $FONT -ta 'r' -bg $DZEN_BG -fg $DZEN_FG -p -e ''
#tray

