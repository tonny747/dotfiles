#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Set PATH
if [[ $OSTYPE == darwin* ]]; then
    export PATH=/usr/local/bin:~/Library/Python/2.7/bin:$PATH
fi

# Set emacs client as default editor
EDITOR=ec
VISUAL=ec
