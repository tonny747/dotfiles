(add-to-list 'load-path "~/.emacs.d/lisp/use-package")
(add-to-list 'load-path "~/.emacs.d/site-lisp")
(setenv "LANG" "en_AU.UTF-8")
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(require 'package)

;; (package-initialize)
(require 'use-package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
;; Set some defaults
(when (display-graphic-p)
 (setq frame-title-format '(buffer-file-name "%f" ("%b")))
 (blink-cursor-mode -1)
 (add-hook 'after-init-hook 'server-start t))



(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(ansi-color-for-comint-mode-on)

(setq sh-indentation 2)
(setq indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)


(display-time-mode t)
(setq display-time-day-and-date t
      display-time-24hr-format t)
(display-time)
(tool-bar-mode -1)
(column-number-mode t)
(global-linum-mode 1)
(put 'downcase-region 'disabled nil)
(setq inhibit-startup-screen t)
(show-paren-mode t)
(desktop-save-mode 1)
(defalias 'yes-or-no-p 'y-or-n-p)
(winner-mode 1)
(scroll-bar-mode -1)
(global-font-lock-mode t)
(transient-mark-mode t)

(delete-selection-mode 1)

(electric-pair-mode +1)
(electric-indent-mode +1)

;; Spelling
(setq ispell-program-name "aspell")

;; Visual bell appears to cause issues with scrolling in OS X
(if (eq system-type 'darwin)
    (progn
      (setq ring-bell-function 'ignore)
      (setq ispell-program-name "/usr/local/bin/aspell"))
  (setq visual-bell 1))

(setq ispell-dictionary "english")

;; Save a list of recent files visited.
(recentf-mode 1)
(setq recentf-keep '(file-remote-p file-readable-p))

(setq backup-directory-alist `(("." . ,(expand-file-name "~/.emacs.d/backups"))))

;; Unbind Pesky Sleep Button
(global-unset-key [(control z)])
(global-unset-key [(control x)(control z)])

;; Theme/font
(load-theme 'tango-dark)
(setq initial-frame-alist '(
                            (font . "Inconsolata-g-10")))
(setq default-frame-alist '(
                            (font . "Inconsolata-g-10")))

;; Got this from:
;;   http://emacs-fu.blogspot.com/2009/12/changing-cursor-color-and-shape.html

;; Change cursor color according to mode; inspired by
;; http://www.emacswiki.org/emacs/ChangingCursorDynamically
(setq djcb-read-only-color       "gray")
;; valid values are t, nil, box, hollow, bar, (bar . WIDTH), hbar,
;; (hbar. HEIGHT); see the docs for set-cursor-type

(setq djcb-read-only-cursor-type 'hbar)
(setq djcb-overwrite-color       "red")
(setq djcb-overwrite-cursor-type 'box)
(setq djcb-normal-color          "yellow")
(setq djcb-normal-cursor-type    'bar)

(defun djcb-set-cursor-according-to-mode ()
  "change cursor color and type according to some minor modes."

  (cond
    (buffer-read-only
      (set-cursor-color djcb-read-only-color)
      (setq cursor-type djcb-read-only-cursor-type))
    (overwrite-mode
      (set-cursor-color djcb-overwrite-color)
      (setq cursor-type djcb-overwrite-cursor-type))
    (t
      (set-cursor-color djcb-normal-color)
      (setq cursor-type djcb-normal-cursor-type))))

(add-hook 'post-command-hook 'djcb-set-cursor-according-to-mode)

;; C/C++ defaults
;; CC-mode
(setq tab-width 2)

(add-hook 'c-mode-common-hook '(lambda ()
        ;; (setq ac-sources (append '(ac-source-semantic) ac-sources))
        (local-set-key (kbd "RET") 'newline-and-indent)
        (linum-mode t)
        (flymake-mode 0)
        (setq c-default-style "bsd")
        (setq c-basic-offset 4)
        (c-set-offset 'case-label '+)
        (setq indent-tabs-mode nil)
        (global-whitespace-mode)
        ;; Set it so labels like public: and private: get tabed out
        (c-set-offset 'access-label 0)
        ;; (semantic-mode t)
        ;; (global-semantic-decoration-mode 1)
        ;; (global-semantic-highlight-edits-mode 1)
        ;; (global-semantic-highlight-func-mode 1)
        ;; (global-semantic-idle-breadcrumbs-mode 1)
        ;; (global-semantic-idle-completions-mode 1)
        ;; (global-semantic-idle-local-symbol-highlight-mode 1)
        ;; (global-semantic-idle-scheduler-mode 1)
        ;; (global-semantic-idle-summary-mode 1)
        ;; (global-semantic-mru-bookmark-mode 1)
        ;; (global-semantic-show-parser-state-mode 1)
        ;; (global-semantic-show-unmatched-syntax-mode 1)
        ;; (global-semantic-stickyfunc-mode 1)
        ;; (global-semanticdb-minor-mode 1)
))
;; From http://stackoverflow.com/questions/7189742/c-c-mode-in-emacs-change-color-of-code-within-if-0-endif-block

(defun cpp-highlight-if-0/1 ()
  "Modify the face of text in between #if 0 ... #endif."
  (interactive)
  (setq cpp-known-face '(forground-color . "dim gray"))
  (setq cpp-unknown-face 'default)
  (setq cpp-face-type 'dark)
  (setq cpp-known-writable 't)
  (setq cpp-unknown-writable 't)
  (setq cpp-edit-list
        '((#("1" 0 1
             (fontified nil))
           nil
           (foreground-color . "dim gray")
           both nil)
          (#("0" 0 1
             (fontified nil))
           (foreground-color . "dim gray")
           nil
           both nil)
          (#("00" 0 1
             (fontified nil))
           (foreground-color . "dim gray")
           nil
           both nil)))
  (cpp-highlight-buffer t))

(setq c-basic-offset 4)

;; Python
(add-hook 'python-mode-hook
      (lambda ()
        (setq indent-tabs-mode)
        (setq tab-width 2)
        (setq python-indent-offset 2)
        ))

;; Packages
(use-package saveplace
  :init
  (setq save-place-file "~/.emacs.d/saveplace")
  (setq-default save-place t)
)

(use-package pbcopy
  :if (eq system-type 'darwin)
  :config
  (turn-on-pbcopy))

(use-package magit
  :bind ("C-x g" . magit-status))

(use-package ido
  :config
  (ido-mode))

(use-package web-mode
  :load-path "site-lisp/web-mode"
  :init
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
  )

(use-package js2-mode
  :load-path "site-lisp/js2-mode"
  :mode "\\.js\\'")

(use-package fic-mode
  :load-path "lisp/fic-mode"
  :init
  (add-hook 'prog-mode-hook 'fic-mode))

(use-package flyspell
  :config
  (dolist (hook '(text-mode-hook))
    (add-hook hook (lambda () (flyspell-mode 1))))
  (dolist (hook '(change-log-mode-hook log-edit-mode-hook))
    (add-hook hook (lambda () (flyspell-mode -1))))
  (add-hook 'c++-mode-hook 'flyspell-prog-mode))

(use-package whitespace
  :config
  (setq whitespace-style '(face tabs lines-tail trailing))
  (global-whitespace-mode))

  ;; undo tree
(use-package undo-tree
  ;; :ensure t
  :config 
  (undo-tree-history-directory-alist (quote (("." . "~/.emacs.d/undo-trees"))))
  (global-undo-tree-mode)
  (setq undo-tree-auto-save-history 1))

(use-package buffer-move)

(use-package dockerfile-mode
  :mode "Dockerfile\\'"
  )

(use-package rainbow-delimiters
  :config
  (rainbow-delimiters-mode))

(use-package sws-mode
  :load-path "site-lisp/jade-mode")

(use-package jade-mode
  :load-path "site-lisp/jade-mode"
  :mode "\\.jade\\'")
(use-package restclient)

(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode))

(use-package company-tern
  :config
  (add-to-list 'company-backends 'company-tern))

(use-package markdown-mode)
;; Misc functions

;; Original idea from
;; http://www.opensubscriber.com/message/emacs-devel@gnu.org/10971693.html
(defun comment-dwim-line (&optional arg)
  "Replacement for the comment-dwim command.
        If no region is selected and current line is not blank and we are not at the end of the line,
        then comment current line.
        Replaces default behaviour of comment-dwim, when it inserts comment at the end of the line."
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    (comment-dwim arg)))
(global-set-key "\M-;" 'comment-dwim-line)

(defun find-alternate-file-with-sudo ()
  "Open current file with sudo"
  (interactive)
  (find-alternate-file (concat "/sudo::" (buffer-file-name))))

(bind-key "C-x C-v" #'find-alternate-file-with-sudo)

(when (file-exists-p "~/.emacs.d/lisp/work.el")
  (load "~/.emacs.d/lisp/work.el" nil 'noerror))
